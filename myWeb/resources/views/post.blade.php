@extends('layout')

@section('content')
    <h1>{{ $post->title }}</h1>
    <h2>{{ $post->content }}</h2>

    <h3>comments</h3>
    <ul>
    @foreach($comments as $comment)
        <li>{{ $comment->content }}</li>
    @endforeach
    </ul>

    <form method="POST" action="/createC">
        {{ csrf_field() }}
        <input type="hidden" name="postId" value="{{$post->id}}">
        <div>
            <textarea name="comment" placeholder="content"></textarea>
        </div>

        <div>
            <button type="submit">submit</button>
        </div>
    </form>

@endsection