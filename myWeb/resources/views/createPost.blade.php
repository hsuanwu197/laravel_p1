@extends('layout')

@section('content')
    <h1>create</h1>
    <form method="POST" action="createPost">

        {{ csrf_field() }}
        <div>
            <input type="text" name="title" placeholder="Post title">
        </div>    

        <div>
            <textarea name="content" placeholder="Post content"></textarea>
        </div>

        <div>
            <button type="submit">submit</button>
        </div>
    </form>
@endsection