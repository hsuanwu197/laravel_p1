@extends('layout')

@section('title')
    welcome
@endsection

@section('content')
    <h1>welcome</h1>
    <p> <a href = "/create">create</a></p>
    <ul>
    @foreach($posts as $post)
        <li><a href = "/post/{{ $post->id }}">{{ $post->title }} {{ \Carbon\Carbon::parse($post->created_at)->format('Y/m/d H:i:s')}}</a></li>
    @endforeach
    </ul>

@endsection