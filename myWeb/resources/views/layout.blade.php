<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title', 'default')</title>
    </head>
    <body>
    <link rel="stylesheet" href="/css/test.css">
        @yield('content')
    </body>
</html>
