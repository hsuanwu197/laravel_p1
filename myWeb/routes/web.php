<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PostController@welcome');
Route::get('/post/{postId}', 'PostController@showPost');
Route::get('/create', 'PostController@create');
Route::post('/createPost', 'PostController@store');
Route::post('/createC', 'PostController@comment');
