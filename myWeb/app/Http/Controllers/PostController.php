<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{
    private $watchingPost = 0;
    
    public function welcome()
    {
        $posts = \App\Posts::select('id','title','content' ,'created_at')->get();

        $collect = collect($posts)->reject(function ($item) {
            return !($item->content and $item->title);
        });

        return view('welcome')->with([
            'posts' => $collect
        ]);
    }

    public function showPost($id){
        $post = \App\Posts::where('id', $id)->get();
        $comments = \App\Comments::select('content')->where('postId', $id)->get();
        return view('post')->withPost($post[0])->withComments($comments);
    }

    public function create(){
        return view('createPost');
    }

    public function store(){
        $post = new \App\Posts();
        if (strlen(request('title')) > 5){
            return redirect('/create');
        }
        else{
            $post->title = request('title');
            $post->content = request('content');
            $post->id = count(\App\Posts::all()) + 1;
            $post->save();
            return redirect('/');
        }
    }
    public function comment(){
        $comment = new \App\Comments();
        if (request('comment')){
            $comment->postId = request('postId');
            $comment->content = request('comment');
            $comment->id = count(\App\Comments::all()) + 1;
            $comment->save();
            echo $comment;
        }

        return redirect('/post/' . request('postId'));
    }
}
