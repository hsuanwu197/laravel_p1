<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('posts')->insert([
            'id' => 1,
            'title' => 'post1',
            'content' => 'c1111111',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('posts')->insert([
            'id' => 2,
            'title' => 'post2',
            'content' => 'c222222',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('posts')->insert([
            'id' => 3,
            'title' => 'post3',
            'content' => 'c333333',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('posts')->insert([
            'id' => 4,
            'title' => '',
            'content' => 'c4444',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);


        DB::table('posts')->insert([
            'id' => 5,
            'title' => 'post5',
            'content' => '',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);



    }
}
