<?php

use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('comments')->insert([
            'id' => 1,
            'postId' => 1,
            'content' => 'aaaaaa',
        ]);
        
        DB::table('comments')->insert([
            'id' => 2,
            'postId' => 1,
            'content' => 'vvvbbb',
        ]);
        DB::table('comments')->insert([
            'id' => 3,
            'postId' => 1,
            'content' => 'eeee',
        ]);
        DB::table('comments')->insert([
            'id' => 4,
            'postId' => 2,
            'content' => 'wewewe',
        ]);
        DB::table('comments')->insert([
            'id' => 5,
            'postId' => 2,
            'content' => 'eeeeewww',
        ]);
        DB::table('comments')->insert([
            'id' => 6,
            'postId' => 3,
            'content' => 'qqqqq',
        ]);
    }
}
